import img1 from "./assests/img1.jpeg";
import img2 from "./assests/img2.jpeg";
import img7 from "./assests/img7.jpeg";
import img4 from "./assests/img4.jpeg";
import download from "./assests/Downloads-icon.png";
import img6 from "./assests/img6.jpeg";
let resumeData = {
    "imagebaseurl":"https://rbhatia46.github.io/",
    "name": "Shubham Jayswal",
    "role": "Full Stack Developer in JavaScript",
    "linkedinId":"Your LinkedIn Id",
    "skypeid": "Your skypeid",
    "emailid":"Your emailid",
    "img":download,
    "roleDescription": "I like to learn about new technologies and play games in my free time.",
    "socialLinks":[
        {
          "name":"linkedin",
          "url":"https://www.linkedin.com/in/subham-jayswal-3b8476150/",
          "className":"fa fa-linkedin"
        },
        {
          "name":"github",
          "url":"https://github.com/shubhamroy7225",
          "className":"fa fa-github"
        },
        {
          "name":"skype",
          "url":"https://twitter.com/shubham93100786",
          "className":"fa fa-twitter"
        },
        {
          "name":"facebook",
          "url":"https://www.facebook.com/shubham.jayswal.33886",
          "className":"fa fa-facebook"
        }
      ],
    "aboutme":"I am software engineer.I have done Master of computer application(M.C.A) in computer science from RGPV university indore(M.P). I am a self taught Full Stack Web Developer. I believe that to be successful in life, one needs to be obsessive with their dreams and keep working towards them. I have a 7-month experience, previously I have worked with Full-Stack-Developer in JavaScript.I know NodeJS, ExpressJS as a back-end, and for front-end, I know ReactJS, Redux, HTML, CSS, Bootstrap, material-UI, and for databases, I know SQL, MySQL, PostgreSQL, and MongoDB.",
    "address":"India",
    "website":"https://rbhatia46.github.io",
    "education":[
      {
        "UniversityName":"The LNM Insitute of Information Technology",
        "specialization":"Some specialization",
        "MonthOfPassing":"Aug",
        "YearOfPassing":"2020",
        "Achievements":"Some Achievements"
      },
      {
        "UniversityName":"Some University",
        "specialization":"Some specialization",
        "MonthOfPassing":"Jan",
        "YearOfPassing":"2018",
        "Achievements":"Some Achievements"
      }
    ],
    
    "portfolio":[
      {
        "name":"Memory Game",
        "description":"",
        "imgurl":img1,
        "link":"https://young-ridge-39899.herokuapp.com/"
      },
      {
        "name":"Trello clone",
        "description":"",
        "imgurl":img2,
        "link":"https://master.d2sco1xx3h73m3.amplifyapp.com/"
      },
      {
        "name":"Our pricing",
        "description":"",  
        "imgurl":img7,
        "link":"https://still-taiga-07245.herokuapp.com/"
      },
      {
        "name":"Redbus clone",
        "description":"",
        "imgurl":img4,
        "link":"https://ticket-feature.d2f1dh61own7gs.amplifyapp.com/"
      },
      {
        "name":"Messenger clone",
        "description":"",
        "imgurl":img6,
        "link":"https://messenger-clone-e9a13.web.app/"
      },
      
    ],
    
  }
  
  export default resumeData